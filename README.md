# A Technical Documentation Archive by Niamh Brown #

## Introduction ##

**I have been developing enterprise level backend systems for over 18 years.** Throughout this time I have been designing, documenting, and diagramming many mission critical systems. I've experience integrating these systems with external APIs of all forms and qualities. So I understand documentation from the user's perspective. Further I have developed bespoke APIs that included in-house documentation and integration support.

**In December 2017 I got a call from Talent Acquisition at [Unity](https://unity3d.com) about a position that had opened up as a Technical Writer for the Core Engine team.** During this process I realised I didn't have a collected portfolio of any of my documentation. Apart from a short stint in 2013-2014, I never considered technical writing as an actual profession. I had been coding for too many years. Most people who I was in contact with wanted to hire me as a developer. The technical writing was an added bonus for them (and for me as I enjoy doing it). So I never had a need to build a portfolio of my documentation. Worse than that, the instinct to preserve and archive my work was not at the top of my priority list. So I had little at hand to give to Unity.

**So I spent a weekend going through years of emails, attachments, google docs, sketchbooks, and archive files of old cd backups to find some examples.** Although what I present here is the tip of the iceberg, as I wasn't able to recover much considering what I have created and contributed to over the years, but I hope it gives you a taste of what I am capable of as a technical writer. The vast majority of my best work resides behind login screens and in restricted wikis, so I can no longer access them. Over time I intend to add better examples to give a clearer idea of the range of my ability.

**Most of this work below comes from**: in-house gaming projects under my husbands business [Hermit Studio](http://www.hermitstudio.com), my dissertation and other university assignments, and some projects I did under contract with other companies (although I've removed any identifying or protected data as best I could).

**Please keep in mind that some of this work is old.** Technology and and my writing capability have come a long way since I created some of these documents. I've not edited my work (e.g. corrected typos etc.), but left it in it's original state. This is so you can see the progression of my writing over time.

**So welcome to my work :)**

* [Quick Link to Full Document List](https://bitbucket.org/NiamhBrown/tech-doc-archive/src/b91949887c01de10c2f9d1096eff20b8404b5590/docs/?at=master) (Note: if any links below are broken then look in here instead.)

----

## Hermit Studio ##

### Cyrano Against the One Hundred ###

**Technical Level**: Advanced
**Draft Quality**: Final Draft
**Created**: 2011-2012

*Cyrano Against the One Hundred was a game concept that my partner and I developed, depicting the famous battle won by Cyrano de Bergerac against one hundred assassins in one night. We wanted to use this battle as the basis of a combat game set in the streets of France. As you can see in this document we had fleshed out the game mechanics for developing it in Unity. I took concepts and notes from many sessions and sources and consistency brought them together into one coherent document over a period of months.*

* [Cyrano Game Mechnanics](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/Cyrano/Cyrano%20Game%20Mechanics%20v1.0.pdf)

----

### Lost Worlds Online ###

**Technical Level**: Intemediate-Advanced
**Draft Quality**: Final Draft
**Created**: 2004-2005

*My partner and I decided to adapt the Lost Worlds turn based gladiatorial book game into a [web game](https://github.com/HermitStudio/LWO). Collected below are some documents and diagrams that I created during this project.*

* [LWO Alpha Framework Activity Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/2005-03-08-LWO-Alpha-Framework-Activity-Diagram.jpg)
* [LWO Overview - Road Map - Individual](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/2005-04-06-Overview-Road-Map-Individual.ods)
* [LWO Rules](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/2005-04-18-LOST%20WORLDS%20RULES.odt)
* [LWO Beginners Guide to the New Results System](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/Beginners%20Guide%20to%20the%20New%20LWO%20Results%20System.odt)
* [LWO New Results System Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/New%20Results%20System.jpg)
* [LWO Sketchbook Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/LWO/Sketchbook-Diagrams-2.JPG)

----

### Online Warfare Pact ###

**Technical Level**: Advanced 
**Draft Quality**: Final Draft
**Created**: 2003-2004

*The OwP was a fair play gaming organisation for the Half-Life mod 'Day of Defeat' which my partner and I set up. The OwP ended up with 9000 members in the days before social media. We were building database driven server tools so you could ensure friendly play on game servers who installed it. This includes the last version of the database schema for the Online Warfare Pact project before it closed down.*

* [Complete OwP Proposed DB Schema](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/OwP/2004-07-28-v3.0-Complete-OwP-Proposed-Schema.png)
* [Complete OwP Proposed DB Schema Printout](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/OwP/OwP-DB-Schema-2.JPG)
* [OwP Signup Storyboard](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/OwP/2004-07-14-OwP-v3.0-Signup-Storyboard.png)
* [OwP Data Dictionary](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/OwP/2004-07-28-OwP-Data-Dictonary-v3.0.doc)

----

### haxAhex ###

**Technical Level**: Beginner
**Draft Quality**: Second Draft
**Created**: 2013

*haxAhex was a puzzle game concept that I created and started to develop in Unity. This document outlined the basic game idea and what I was trying to achieve in the first prototype.*

* [haxAhex Prototype Outline](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/2013-07-21-haxAhex-Prototype-Outline.pdf)

----

### Poker Log Parser ###

**Technical Level**: Beginner 
**Draft Quality**: Final 
**Created**: 2017

*This is an overview of the different tables involved in a small poker log parser. It shows how they related to one other via foreign keys etc. This doesn't show the fields within the tables though.*

* [Poker Table Relations](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/2017-05-31-Poker-Project-Appendix-I-Database-Schema.png)

----

### Stock Game ###

**Technical Level**: Intemediate 
**Draft Quality**: Sketch 
**Created**: 2004

*This is a couple of sketches from a stock game that was parsing real data from the NYSE. The project didn't get very far but these images show my diagrams at an early stage.*

* [Sketchbook Diagram #1](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/Stock%20Game/Sketchbook-Diagrams-1.JPG)
* [Sketchbook Diagram #2](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Hermit%20Studio/Stock%20Game/Sketchbook-Diagrams-3.JPG)

----

## Contactors ##

I have managed to recover a few documents that I could share from the systems I've developed for other companies, or from technical tests taken during the interview process.

### The Red Pill ###

**Technical Level**: Advanced
**Draft Quality**: Final Draft
**Created**: 2015

*I did a test for [Trust Pilot](https://www.trustpilot.com) a few years ago and I created the repository below. I wasn't able to complete the task as well as I'd like but it gave me an excuse to create a ReadMe that would take the reader through what I had attempted to do.*

* [Read TheRedPill ReadMe](https://bitbucket.org/NiamhBrown/theredpill) 

----

### Data Dictionary ###

**Technical Level**: Intemediate
**Draft Quality**: Final Draft
**Created**: 2006

*This was a document mapping out the existing data dictionary of the tables within an educational IT system before redesigning it.*

* [Existing Data Dictionary](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Contractors/2006-08-14-Existing-Data-Dictionary-All.xls)

----

### Database Schema Snippet ###

**Technical Level**: Intemediate-Advanced 
**Draft Quality**: Final Draft (snippet of Whole Schema)
**Created**: 2013

*This was a small part of a larger database schema that had 120+ tables in it. I had to breakdown the whole schema into smaller bite-sized chunks for my colleagues to make it easier to read. This image is one part that I was able to recover of the whole schema. I developed this database over a period of 3 months and it required training and handover sessions for those who needed to learn it within the team.*

* [Database Schema Snippet](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Contractors/Database-Login-and-Categories.png)

----

### Account Database Schema ###

**Technical Level**: Beginner 
**Draft Quality**: Final 
**Created**: 2017

*This was part of a technical test I did for a company during the hiring process. It's a smaller example of a database schema.*

* [Account Database Schema](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Contractors/Account%20-%20Database%20Schema.png)

----

### QuickPay Integration Overview ###

**Technical Level**: Beginner-Intemediate 
**Draft Quality**: Final 
**Created**: 2016

*This was part of a general overview for how the integration with QuickPay was going to work along with a large in-house CRM. It shows the flow of information between related systems.*

* [QuickPay Integration Overview](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Contractors/QuickPay%20Integration.png)

----

## University ##

**Technical Level**: Intermediate-Advanced 
**Draft Quality**: Final Drafts 
**Created**: 2002

*Here are documents and diagrams of the game I developed for my University Dissertation. I was converting the RPG En-garde from pen and paper to a computer game. It also includes some other University assignments.* 

**Reports:**

* [Uni Dissertation](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Reports/2002-04-24-Dissertation.doc)
* [Uni Instruction Manual](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Reports/Instruction%20Manual.doc)
* [Uni Programming Report](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Reports/Programming%20Report%20v1.1.doc)
* [Uni Rapid Application Development Assignment](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Reports/RAD%20Assignment%20-%20Database%20Documentation%202000.doc)
* [Uni Textual Descriptions](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Reports/Textual%20Descriptions%202.1.doc)

**Diagrams:**

* [Uni Class Diagrams](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/2002-01-class-diagram.png)
* [Uni Date Objects](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/2001-11-28-date-objects.png)
* [Uni GE Lobby Sequence Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/2005-06-02-GE-lobby-sequence-diagram.jpg)
* [Uni GE Zone Sequence Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/2005-06-02-GE-zone-sequence-diagram.jpg)
* [Uni Action Tree Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/actiontree.png)
* [Uni Bawdy House Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/bawdyhouse.png)
* [Uni Club Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/club.png)
* [Uni Club Gambling Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/clubgambling.png)
* [Uni Club Loans Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/clubloans.png)
* [Uni Duelling Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/duelling.png)
* [Uni Duelling Conflict Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/duellingconflict.png)
* [Uni Frontier Serve Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/frontierserve.png)
* [Uni Join Club Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/joinclub.png)
* [Uni Mistress Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/mistress.png)
* [Uni Practise Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/practise.png)
* [Uni Recovery Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/recovery.png)
* [Uni Relationships Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/relationships.png)
* [Uni Time Sequence Diagram](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/b91949887c01de10c2f9d1096eff20b8404b5590/docs/Uni/Diagrams/timesequence.png)

----

## Technical Articles ##

I've written some articles over the years exploring various technologies.

### Online Articles ###

Here is a list of articles that are still live on the web:

**CMS Critic:**

**Technical Level**: Beginner-Intermediate
**Draft Quality**: Final Draft
**Created**: 2013

* [How To Set up Google Authorship on a WordPress.com Blog](https://www.cmscritic.com/how-to-set-up-google-authorship-on-a-wordpress-com-blog/)

* [How To Take Advantage Of The Latest Google+ Profile Update - March 2013](https://www.cmscritic.com/how-to-take-advantage-of-the-latest-google-profile-update-march-2013/)

* [Squarespace Review - the Long Awaited Commerce Solution](https://www.cmscritic.com/squarespace-review-the-long-awaited-commerce-solution/)

* [MobStac Review - Manage Mobile Websites and Apps Across All Mobile and Tablet Devices](https://www.cmscritic.com/mobstac-review-manage-mobile-websites-and-apps-across-all-mobile-and-tablet-devices/)

* [AppMachine Review - CMS for App Development](https://www.cmscritic.com/appmachine-beta-review-cms-for-app-development/)

**Degree Search:**

**Technical Level**: Beginner
**Draft Quality**: Final Draft 
**Created**: 2007

* [Things You Should Know Before Pursuing A Degree In Information Technology And Computers](http://www.degreesearch.org/articles/108-information-technology--computers-degree)

### Offline Articles ###

Here is a list of articles that are no longer found on the web, because the website changed or went down. I was able to find a back up of the article before publication. These will not be the final draft but were close enough  for you to get the idea.

**Symphonical:**

**Technical Level**: Beginner-Intemediate
**Draft Quality**: Final Draft (Pre-publication)
**Created**: 2014

* [Symphonical Distributed Teams Article](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/How%20to%20Manage%20a%20Distributed%20Team%20Using%20Symphonical%20by%20Niamh%20Brown-Formatted.rtf)

**Helium:**

**Technical Level**: Beginner
**Draft Quality**: Final Draft (Pre-publication)
**Created**: 2008

* [Front Page: Tips For Cold-Weather Photography with a Digital Camera](https://bytebucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Helium-Cold-Weather-Photography-1.JPG)
* [Understanding Resolution and Picture Quality Settings in Digital Cameras](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Contest_%20Understanding%20resolution%20and%20picture%20quality%20settings%20in.pdf)
* [The Difference Between Web Design and Web Development](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Contest_%20The%20difference%20between%20web%20design%20and%20web%20development.pdf)
* [How to Create an Image Link](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Contest_%20%20How%20to%20create%20an%20image%20link.pdf)
* [How to Learn HTML](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Contest_%20%20How%20to%20learn%20HTML.pdf)
* [Product Reviews: Distributed Organization Tools](https://bitbucket.org/NiamhBrown/tech-doc-archive/raw/d28513dff2179e39bdcca38e216e0281f968843c/docs/Tech%20Articles/Product%20Reviews_%20Distributed%20Organization%20Tools.pdf)

----

### Other Writing ###

As some of you know I have also written and published speculative fiction through the Hermit Studio anthology: [NovoPulp](http://www.novopulp.com).

I have also created all the content for the following websites:

* [Hermit Studio](http://hermitstudio.com)
* [Hermit House](http://hermit.house)
* [Poultry Conservancy](http://poultryconservancy.org) WIP!

----

### Contact ###

* Niamh Brown @ [Hermit Studio](http://hermitstudio.com)